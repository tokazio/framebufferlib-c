/* 
 * File:   FrameBuffer.h
 * Author: root
 *
 * http://www.ummon.eu/Linux/API/Devices/framebuffer.html
 * 
 * https://github.com/OtherCrashOverride/HelloGE2D
 * 
 * Created on 11 février 2016, 09:18
 */

#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <fcntl.h> 
#include <unistd.h> 
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/kd.h>
#include <string.h>
#include "Rgba.h"
#include <errno.h>

   #include "ge2d.h" 
    
    //============================================================================
    //============================================================================
    //============================================================================
    // FRAMEBUFFER OPTIONS
    //============================================================================
    //============================================================================
    //============================================================================    

    extern int fb_open(char* const fbFile);
    extern void fb_close(void);
    extern void fb_print_info(void);
    extern void fb_hide_cursor(void);
    extern void fb_show_cursor(void);
    extern void fb_page_flip(void);
    extern int fb_screen_width(void);
    extern int fb_screen_height(void);
    extern int fb_pixel_size(void);
    extern int fb_line_size(void);
    extern int fb_bpp(void);
    extern int fb_page_size(void);
    extern int fb_page_start(void);
    extern int fb_xoffset(void);
    extern int fb_yoffset(void);
    //extern void fb_put(int location, rgba_t* c);
    extern void fb_put_at(int location, unsigned char* c);
    //extern rgba_t* fb_get(int location);
    extern void fb_get_at(int location, unsigned char* c);    
    extern void fb_clear(rgba_t* c);
    
    extern void fb_ge2d_fill_rect(int x, int y, int width, int height, rgba_t* color);
    extern void fb_ge2d_blit(int dstX, int dstY);

    extern char* fb_last_error_str(void);
    
#ifdef __cplusplus
}
#endif

#endif /* FRAMEBUFFER_H */


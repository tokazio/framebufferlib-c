#include "Rgba.h"

//RGBA 32bpp

/**
 * 
 * @param a
 * @param b
 * @return 
 */
int rgba_compare(rgba_t * const a, rgba_t * const b) {
    if (a == NULL) return 0;
    if (b == NULL) return 0;
    return (a->full[2] == b->full[2] && a->full[1] == b->full[1] && a->full[0] == b->full[0] && a->full[3] == b->full[3]);
}

/**
 * @todo: to char array! (put it directly in framebuffer)
 * @param r
 * @param g
 * @param b
 * @param a
 * @return 
 */
rgba_t* mapRgba(unsigned char const r, unsigned char const g, unsigned char const b, unsigned char const a) {
    rgba_t* c = malloc(sizeof (rgba_t));
    //
    c->full[0] = b;
    c->full[1] = g;
    c->full[2] = r;
    c->full[3] = a;
    //Pré alpha blending
    rgba_alpha_blend(c, a);
    //
    return c;
}

void rgba_alpha_blend(rgba_t * c, int const a) {
    c->ra = c->full[2]*a;
    c->ga = c->full[1]*a;
    c->ba = c->full[0]*a;
    c->aa = 0xFF - a; //256-a
}

/**
 * 
 * @param rbga
 */
void unmapRgba(rgba_t* col) {
    free(col);
}

/**
 * 
 * @param r
 * @param g
 * @param b
 * @return 
 */
rgba_t* mapRgb(unsigned char const r, unsigned char const g, unsigned char const b) {
    return mapRgba(r, g, b, 0xFF);
}

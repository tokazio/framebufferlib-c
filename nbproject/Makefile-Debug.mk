#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/FbDraw.o \
	${OBJECTDIR}/FbJpg.o \
	${OBJECTDIR}/FbText.o \
	${OBJECTDIR}/FrameBuffer.o \
	${OBJECTDIR}/Rgba.o \
	${OBJECTDIR}/TouchScreen.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lm -ljpeg -pthread -lfreetype

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libframebuffer.${CND_DLIB_EXT}

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libframebuffer.${CND_DLIB_EXT}: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libframebuffer.${CND_DLIB_EXT} ${OBJECTFILES} ${LDLIBSOPTIONS} -shared -fPIC

${OBJECTDIR}/FbDraw.o: FbDraw.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -I/usr/include/freetype2 -std=c99 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/FbDraw.o FbDraw.c

${OBJECTDIR}/FbJpg.o: FbJpg.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -I/usr/include/freetype2 -std=c99 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/FbJpg.o FbJpg.c

${OBJECTDIR}/FbText.o: FbText.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -I/usr/include/freetype2 -std=c99 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/FbText.o FbText.c

${OBJECTDIR}/FrameBuffer.o: FrameBuffer.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -I/usr/include/freetype2 -std=c99 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/FrameBuffer.o FrameBuffer.c

${OBJECTDIR}/Rgba.o: Rgba.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -I/usr/include/freetype2 -std=c99 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Rgba.o Rgba.c

${OBJECTDIR}/TouchScreen.o: TouchScreen.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -I/usr/include/freetype2 -std=c99 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/TouchScreen.o TouchScreen.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libframebuffer.${CND_DLIB_EXT}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc

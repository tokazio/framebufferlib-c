/* 
 * File:   FbJpg.h
 * Author: root
 *
 * http://stackoverflow.com/questions/694080/how-do-i-read-jpeg-and-png-pixels-in-c-on-linux
 * 
 * Created on 11 février 2016, 12:27
 */

#ifndef FBJPG_H
#define FBJPG_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include "FbDraw.h"
#include "Rgba.h"
#include <math.h>

    /*
     * Include file for users of JPEG library.
     * You will need to have included system headers that define at least
     * the typedefs FILE and size_t before you can include jpeglib.h.
     * (stdio.h is sufficient on ANSI-conforming systems.)
     * You may also wish to include "jerror.h".
     */
#include <jpeglib.h>
    /*
     * <setjmp.h> is used for the optional error recovery mechanism shown in
     * the second part of the example.
     */
#include <setjmp.h>

    /*
     * ERROR HANDLING:
     *
     * The JPEG library's standard error handler (jerror.c) is divided into
     * several "methods" which you can override individually.  This lets you
     * adjust the behavior without duplicating a lot of code, which you might
     * have to update with each future release.
     *
     * Our example here shows how to override the "error_exit" method so that
     * control is returned to the library's caller when a fatal error occurs,
     * rather than calling exit() as the standard error_exit method does.
     *
     * We use C's setjmp/longjmp facility to return control.  This means that the
     * routine which calls the JPEG library must first execute a setjmp() call to
     * establish the return point.  We want the replacement error_exit to do a
     * longjmp().  But we need to make the setjmp buffer accessible to the
     * error_exit routine.  To do this, we make a private extension of the
     * standard JPEG error handler object.  (If we were using C++, we'd say we
     * were making a subclass of the regular error handler.)
     *
     * Here's the extended error handler struct:
     */

    struct my_error_mgr {
        struct jpeg_error_mgr pub; /* "public" fields */

        jmp_buf setjmp_buffer; /* for return to caller */
    };

    typedef struct my_error_mgr * my_error_ptr;

    typedef struct fb_jpg_image {
        rgba_t* (*data);
        int w;
        int h;
        char* name;
    } fb_jpg_image;


    fb_jpg_image* _fb_jpg_image(void);

    void fb_jpg_image_(fb_jpg_image* image);

    fb_jpg_image* fb_jpg_image_load_file(const char* name);

    void fb_fill_jpg_at(fb_jpg_image * const image, int const x0, int const y0);

    void fb_fill_jpg_at_size(fb_jpg_image * const image, int const x0, int const y0, int const width, int const height);

    void fb_fill_jpg_at_scale(fb_jpg_image * const image, int const x0, int const y0, double const xscale, double const yscale);

#ifdef __cplusplus
}
#endif

#endif /* FBJPG_H */


#include "FrameBuffer.h"

static int kbfd;
static struct fb_fix_screeninfo fscreeninfo;
static struct fb_var_screeninfo vscreeninfo;
static int fbfd;
static int fd_ge2d;
static int fd_mem;
static unsigned char *fbp;
static int isVsyncSupported;
static int isPanSupported;
static int cur_page = 0;
static int page_size;
static int* data_mem;
static int length_mem;

// /dev/fb1 memory location from dmesg output
// mesonfb1(low)           : 0x07900000 - 0x07a00000 (1M)
const unsigned long physicalAddress = 0x07900000;

/**
 * To be passed in vsync fb function (need to be a pointer to work)
 */
static __u32 dummy = 0;

char* fb_last_error_str(void){
    return strerror(errno);
}

/**
 * Open cursor device.
 * 
 * @return 0 if ok, else 1 (device not opened)
 */
static int fb_open_cursor(void) {
    kbfd = open("/dev/tty", O_WRONLY);
    if (kbfd < 0) {
        printf("Could not open %s. (%s)\n", "/dev/tty", strerror(errno));
        return (1);
    }
    return 0;
}

/**
 * Close cursor device.
 */
static void fb_close_cursor(void) {
    close(kbfd);
}

/**
 * Cursor GRAPHIC.
 */
void fb_hide_cursor(void) {
    if (kbfd >= 0) {
        ioctl(kbfd, KDSETMODE, KD_GRAPHICS);
    }
}

/**
 * Cursor TEXT.
 */
void fb_show_cursor(void) {
    if (kbfd >= 0) {
        ioctl(kbfd, KDSETMODE, KD_TEXT);
    }
}

/**
 * 
 * @return 
 */
int fb_screen_width(void) {
    return vscreeninfo.xres;
}

/**
 * 
 * @return 
 */
int fb_screen_height(void) {
    return vscreeninfo.yres;
}

/**
 * 
 * @return 
 */
int fb_pixel_size(void) {
    return fb_bpp() / 8;
}

/**
 * 
 * @return 
 */
int fb_line_size(void) {
    return fscreeninfo.line_length;
}

/**
 * 
 * @return 
 */
int fb_bpp(void) {
    return vscreeninfo.bits_per_pixel;
}

/**
 * 
 * @return 
 */
int fb_page_size(void) {
    return page_size;
}

/**
 * 
 * @return 
 */
int fb_page_start(void) {
    return fb_page_size() * cur_page;
}

/**
 * 
 * @return 
 */
int fb_xoffset(void) {
    return vscreeninfo.xoffset;
}

/**
 * 
 * @return 
 */
int fb_yoffset(void) {
    return vscreeninfo.yoffset;
}

/**
 * 
 * @param location
 * @param c
 */
void fb_put_at(int location, unsigned char* c) {
    __builtin_memmove(fbp + location, c, sizeof (c));
}

#include <stdint.h>
/*
void memcopy(void* d, void* s, int w){
  uint8_t* dst = d;
  const uint8_t* src = s;
  int remaining = w;
  __asm__ __volatile__ (
    "1:                                               \n"
    "subs     %[rem], %[rem], #32                     \n"
    "vld1.u8  {d0, d1, d2, d3}, [%[src],:256]!        \n"
    "vst1.u8  {d0, d1, d2, d3}, [%[dst],:256]!        \n"
    "bgt      1b                                      \n"
    : [dst]"+r"(dst), [src]"+r"(src), [rem]"+r"(remaining)
    :
    : "d0", "d1", "d2", "d3", "cc", "memory"
  );
}
 * */

/**
 * 
 * @param location
 * @param y
 * @param w
 * @param h
 * @param c
 */
void fb_put_rect(int location, int const w, int const h, unsigned char* c) {
    int j = 0;
    int k = 0;
    int size = 4 * w; //fb_pixel_size * w;
    unsigned char line[size];
    //One row
    while (j < size) {
        __builtin_memmove(&line[j], c, 4);
        j += 4;
    }
    //Rows
    while (k < h) {
        __builtin_memmove(fbp + location, line, size);
        location += fb_line_size();
        k++;
    }

}

/**
 * 
 * @param location
 * @param y
 * @param w
 * @param c
 */
void fb_put_line(int location, int const w, unsigned char* c) {
    int j = 0;
    int size = 4 * w; //fb_pixel_size * w;
    //One row
    while (j < size) {
        __builtin_memmove(fbp + location, c, 4);
        j += 4;
    }
}

/**
 * 
 * @param location
 * @param c
 */
void fb_get_at(int location, unsigned char* c) {
    __builtin_memmove(c, fbp + location, sizeof (c));
}

/**
 * 
 * @param c
 * @todo: tenir compte de la couleur
 */
void fb_clear(rgba_t* c) {
    if (c == 0) {
        __builtin_memset(fbp + fb_page_start(), 0, fb_page_size());
    } else {
        int p = fb_page_start();
        while (p < fb_page_size()) {
            __builtin_memmove(fbp + p, &c->full, 4);
            p += 4;
        }
    }
}

/**
 * 
 * @return 
 */
static int fb_ge2d_open(void) {
    length_mem = fb_screen_width() * fb_screen_height() * 4;
    fd_ge2d = open("/dev/ge2d", O_RDWR);
    if (fd_ge2d < 0) {
        printf("Error: cannot open ge2d device. (%s)\n", strerror(errno));
        return (1);
    }
    fd_mem = open("/dev/mem", O_RDWR | O_SYNC);
    if (fd_mem < 0) {
        printf("Error: cannot open mem device. (%s)\n", strerror(errno));
        return (2);
    }

    data_mem = (int*) mmap(0, length_mem, PROT_READ | PROT_WRITE, MAP_SHARED, fd_mem, physicalAddress);
    if (data_mem == NULL) {
        printf("Error: cannot map mem device. (%s)\n", strerror(errno));
        return (3);
    }
    printf("virtual mem device address = %x\n", data_mem);
}

/**
 * 
 */
static void fb_ge2d_close() {
    if (fd_ge2d > 0) {
        close(fd_ge2d);
        printf("ge2d device closed.\n");
        //
        munmap(data_mem, length_mem);
        close(fd_mem);
        printf("mem device closed.\n");
    }
}

void fb_ge2d_fill_rect(int x, int y, int width, int height, rgba_t* color) {
    if (fd_ge2d > 0) {
        // Tell the hardware the destination is /dev/fb0
        config_para_t config;
        memset(&config, 0x00, sizeof (config));

        config.src_dst_type = OSD0_OSD0;

        int ret = ioctl(fd_ge2d, GE2D_CONFIG, &config);
        printf("GE2D_CONFIG ret: %x\n", ret);


        // Perform a fill operation;
        ge2d_para_t fillRectParam;
        fillRectParam.src1_rect.x = x;
        fillRectParam.src1_rect.y = y;
        fillRectParam.src1_rect.w = width;
        fillRectParam.src1_rect.h = height;
        fillRectParam.color = (int) color->full; // R G B A

        ret = ioctl(fd_ge2d, GE2D_FILLRECTANGLE, &fillRectParam);
        printf("GE2D_FILLRECTANGLE ret: %x\n", ret);
    }
}

void fb_ge2d_blit(int dstX, int dstY) {
    if (fd_ge2d > 0) {
        // Tell the hardware we will source memory (that we borrowed)
        // and write to /dev/fb0

        // This shows the expanded form of config.  Using this expanded
        // form allows the blit source x and y read directions to be specified
        // as well as the destination x and y write directions.  Together
        // they allow overlapping blit operations to be performed.
        config_para_ex_t configex;
        memset(&configex, 0x00, sizeof (configex));

        configex.src_para.mem_type = CANVAS_ALLOC;
        configex.src_para.format = GE2D_FORMAT_S32_RGBA;
        configex.src_planes[0].addr = (unsigned long) physicalAddress;
        configex.src_planes[0].w = fb_screen_width();
        configex.src_planes[0].h = fb_screen_height();
        configex.src_para.left = 0;
        configex.src_para.top = 0;
        configex.src_para.width = fb_screen_width();
        configex.src_para.height = fb_screen_height();
        configex.src_para.x_rev = 0;
        configex.src_para.y_rev = 0;

        configex.src2_para.mem_type = CANVAS_TYPE_INVALID;

        configex.dst_para.mem_type = CANVAS_OSD0;
        configex.dst_para.left = 0;
        configex.dst_para.top = 0;
        configex.dst_para.width = fb_screen_width();
        configex.dst_para.height = fb_screen_height();
        configex.dst_para.x_rev = 0;
        configex.dst_para.y_rev = 0;


        int ret = ioctl(fd_ge2d, GE2D_CONFIG_EX, &configex);
        printf("GE2D_CONFIG_EX ret: %x\n", ret);


        // Perform the blit operation
        ge2d_para_t blitRectParam2;
        memset(&blitRectParam2, 0, sizeof (blitRectParam2));

        blitRectParam2.src1_rect.x = 0;
        blitRectParam2.src1_rect.y = 0;
        blitRectParam2.src1_rect.w = fb_screen_width();
        blitRectParam2.src1_rect.h = fb_screen_height();
        blitRectParam2.dst_rect.x = dstX;
        blitRectParam2.dst_rect.y = dstY;

        ret = ioctl(fd_ge2d, GE2D_BLIT, &blitRectParam2);
        printf("GE2D_BLIT ret: %x\n", ret);
    }
}

/**
 * Open framebuffer device (fbFile).
 * 
 * _ open device (from fbFile)
 * _ get pixels fixed and variables informations
 * _ define page size in case of PAGE_FLIP
 * _ define double buffer in case of DOUBLE_BUFFER
 * _ map device to memory 
 * _ open cursor
 * 
 * @param fbFile
 */
int fb_open(char* const fbFile) {
    // Open the file for reading and writing
    fbfd = open(fbFile, O_RDWR);
    if (fbfd < 0) {
        printf("Error: cannot open framebuffer device. (%s)\n", strerror(errno));
        return (1);
    }
    printf("Framebuffer device opened.\n");

    // Get fixed screen information
    if (ioctl(fbfd, FBIOGET_FSCREENINFO, &fscreeninfo)) {
        printf("Error reading fixed information. (%s)\n", strerror(errno));
        return (2);
    }

    // Get variable screen information
    if (ioctl(fbfd, FBIOGET_VSCREENINFO, &vscreeninfo)) {
        printf("Error reading variable information. (%s)\n", strerror(errno));
        return (3);
    }
    //
    vscreeninfo.xres_virtual = vscreeninfo.xres;
    vscreeninfo.yres_virtual = vscreeninfo.yres * 2; // double the physical  
    if (ioctl(fbfd, FBIOPUT_VSCREENINFO, &vscreeninfo)) {
        printf("Error setting page switch. (%s)\n", strerror(errno));
        return (4);
    }
    page_size = fscreeninfo.line_length * vscreeninfo.yres;

    //pan screen supported ?
    if (ioctl(fbfd, FBIOPAN_DISPLAY, &vscreeninfo)) {
        printf("Panning not supported. (%s)\n", strerror(errno));
        isPanSupported = 0;
    } else {
        isPanSupported = 1;
    }
    //Wait vsync
    if (ioctl(fbfd, FBIO_WAITFORVSYNC, &dummy)) {
        printf("Vsync not supported. (%s)\n", strerror(errno));
        isVsyncSupported = 0;
    } else {
        isVsyncSupported = 1;
    }
    //
    fb_ge2d_open();
    // map framebuffer to user memory 
    fbp = (unsigned char*) mmap(0,
            fscreeninfo.smem_len,
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            fbfd, 0);
    if (fbp < 0) {
        printf("Failed to mmap the framebuffer. (%s)\n", strerror(errno));
        return (5);
    }
    return fb_open_cursor();
}

/**
 * Close framebuffer device (file).
 * 
 * _ unmap
 * _ close
 * _ free double buffer in case of DOUBLE_BUFF
 */
void fb_close(void) {
    // cleanup
    munmap(fbp, fscreeninfo.smem_len);
    close(fbfd);
    fb_close_cursor();
    printf("Framebuffer device closed.\n");
    fb_ge2d_close();
}

/**
 * framebuffer informations
 */
void fb_print_info(void) {

    printf("\nVscreen Info:-\n");
    printf(" Xres   = %4d | Yres   = %4d\n", vscreeninfo.xres, vscreeninfo.yres);
    printf(" BPP    = %4d | Height = %ld | Width = %ld\n", vscreeninfo.bits_per_pixel,
            vscreeninfo.height,
            vscreeninfo.width);
    printf(" Xres_V = %d | Yres_V = %d\n", vscreeninfo.xres_virtual, vscreeninfo.yres_virtual);
    printf(" Pixel format : RGBX_%d%d%d%d\n", vscreeninfo.red.length,
            vscreeninfo.green.length,
            vscreeninfo.blue.length,
            vscreeninfo.transp.length);
    printf(" Begin of bitfields(Byte ordering):-\n"); //In my case :
    printf("  Transp : %d\n", vscreeninfo.transp.offset); //Transp : 24
    printf("  Red    : %d\n", vscreeninfo.red.offset); //Red    : 16
    printf("  Green  : %d\n", vscreeninfo.green.offset); //Green  : 8
    printf("  Blue   : %d\n", vscreeninfo.blue.offset); //Blue   : 0
    //Hence orientation is : BGRT => (BGR4)RGB32 packed format

    printf("\nFscreen Info:-\n");
    printf(" Device ID : %s\n", fscreeninfo.id);
    printf(" Start of FB physical address : %ld\n", fscreeninfo.smem_start);
    printf(" Length of FB : %d\n", fscreeninfo.smem_len); //Size of framebuffer in bytes
    printf(" Length of Line : %d\n", fscreeninfo.line_length);
    printf(" Start of MMIO physical address : %ld\n", fscreeninfo.mmio_start);
    printf(" Length of MMIO : %d\n\n", fscreeninfo.mmio_len);

}

/**
 * Called to render current page
 * 
 * Need to be called after fb_end_draw in case of DOUBLE_BUFF with PAGE_FLIP
 */
void fb_page_flip(void) {
    //pixel page position (page height * page index)
    vscreeninfo.yoffset = cur_page * vscreeninfo.yres;
    //pan screen
    if (isPanSupported) {
        ioctl(fbfd, FBIOPAN_DISPLAY, &vscreeninfo);
    }
    //Wait vsync
    if (isVsyncSupported) {
        ioctl(fbfd, FBIO_WAITFORVSYNC, &dummy);
    }
    //next page
    cur_page = (cur_page + 1) % 2;
}

/* 
 * File:   FbDraw.h
 * Author: root
 * 
 * http://stackoverflow.com/questions/4996777/paint-pixels-to-screen-via-linux-framebuffer
 * http://raspberrycompote.blogspot.fr/2014/04/low-level-graphics-on-raspberry-pi-text.html
 * http://stackoverflow.com/questions/5438313/faster-alpha-blending-using-a-lookup-table
 * 
 * http://clearalgo.com/blog/?p=5
 * 
 * Created on 11 février 2016, 09:19
 */

#ifndef FBDRAW_H
#define FBDRAW_H

#ifdef __cplusplus
extern "C" {
#endif

#include "FrameBuffer.h"
#include "Rgba.h"
#include "FbJpg.h"

    rgba_t* fb_get_pixel(int const x, int const y);
    void fb_draw_pixel(int const x, int const y, rgba_t * const c);
    void fb_fill_rect(int const x, int const y, int const w, int const h, rgba_t * const c);
    void fb_clear_screen(rgba_t * const c);
    void fb_fill_circle(int const x0, int const y0, int const r, rgba_t * const c);
    void fb_stroke_circle(int x0, int y0, int const r, rgba_t * const c);
    void fb_stroke_line(int const x0, int const y0, int const x1, int const y1, rgba_t * const c);
    void fb_fill(int x, int y, rgba_t * const c, rgba_t * const s);

#ifdef __cplusplus
}
#endif

#endif /* FBDRAW_H */


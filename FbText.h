/* 
 * File:   FbText.h
 * Author: root
 *
 * https://gist.github.com/ironpinguin/7627623
 * 
 * Created on 11 février 2016, 12:27
 */

#ifndef FBTEXT_H
#define FBTEXT_H

#ifdef __cplusplus
extern "C" {
#endif


#include <stdio.h>
#include <string.h>
#include <math.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "FbDraw.h"

    typedef struct text_font {
        FT_Face face;
    } text_font_t;

    void text_open(void);
    void text_close(void);
    text_font_t* text_open_font(char* const filename);
    void text_close_font(text_font_t* font);
    void text_fill(text_font_t * const font, int const x, int const y, int const point, char* const text, rgba_t * const col);
    int text_height(text_font_t * const font, int const point, char* const text);
    int text_width(text_font_t * const font, int const point, char* const text);

#ifdef __cplusplus
}
#endif

#endif /* FBTEXT_H */
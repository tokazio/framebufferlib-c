#include "FbText.h"

FT_Library library;

/**
 *
 */
void text_open(void) {
    FT_Error error = FT_Init_FreeType(&library); /* initialize library */
    /* error handling omitted */
}

void text_close(void) {
    FT_Done_FreeType(library);
}

/**
 * 
 * @param filename
 * @return 
 */
text_font_t* text_open_font(char* const filename) {
    text_font_t* f = malloc(sizeof (text_font_t));
    FT_Error error = FT_New_Face(library, filename, 0, &(f->face)); /* create face object */
    /* error handling omitted */
    return f;
}

/**
 * 
 * @param font
 */
void text_close_font(text_font_t* font) {
    FT_Done_Face(font->face);
}

/**
 * 
 * @param bitmap
 * @param x
 * @param y
 * @param col
 */
static void draw_bitmap(FT_Bitmap* bitmap, FT_Int x, FT_Int y, rgba_t * const col) {
    rgba_t c = *col;
    FT_Int i, j, p, q;
    FT_Int x_max = bitmap->width; //x + bitmap->width;
    FT_Int y_max = bitmap->rows; //y + bitmap->rows;
    for (i = 0, p = 0; i < x_max; i++, p++) {
        for (j = 0, q = 0; j < y_max; j++, q++) {
            if (bitmap->buffer[q * bitmap->width + p] > 0) { //alpha of the point
                rgba_alpha_blend(&c, bitmap->buffer[q * bitmap->width + p]);
                fb_draw_pixel(x + i, y + j, &c);
            }
        }
    }
}

/**
 * 
 * @param font
 * @param x
 * @param y
 * @param point
 * @param text
 * @param col
 * @return 
 */
void text_fill(text_font_t * const font, int const x, int const y, int const point, char* const text, rgba_t * const col) {
    FT_GlyphSlot slot;
    //FT_Matrix matrix; /* transformation matrix */
    //double angle = (25.0 / 360) * 3.14159 * 2; /* use 25 degrees     */
    /* set up matrix */
    //matrix.xx = (FT_Fixed) (cos(angle) * 0x10000L);
    //matrix.xy = (FT_Fixed) (-sin(angle) * 0x10000L);
    //matrix.yx = (FT_Fixed) (sin(angle) * 0x10000L);
    //matrix.yy = (FT_Fixed) (cos(angle) * 0x10000L);

    FT_Vector pen; /* untransformed origin  */
    
    printf("FbText.c: text_fill: %s (%d)",text,strlen(text));
    
    int num_chars = strlen(text);
    
    if(num_chars==0)
        return;

    /* @72dpi*/
    FT_Error error = FT_Set_Char_Size(font->face, point * 64, 0, 72, 0); /* set character size */
    /* error handling omitted */

    slot = font->face->glyph;
    /* the pen position */
    pen.x = x;
    pen.y = y;

    for (int n = 0; n < num_chars; n++) {
        /* set transformation */
        //FT_Set_Transform(face, &matrix, &pen);

        /* load glyph image into the slot (erase previous one) */
        error = FT_Load_Char(font->face, text[n], FT_LOAD_RENDER);
        if (error)
            continue; /* ignore errors */

        /* now, draw to our target surface (convert position) */
        draw_bitmap(&slot->bitmap, pen.x + slot->bitmap_left, pen.y - slot->bitmap_top, col);

        /* increment pen position */
        pen.x += slot->advance.x / 64; //slot->bitmap.width;
        //pen.y += slot->bitmap.rows;//slot->advance.y; //new line
    }
}

/**
 * 
 * @param font
 * @param point
 * @param text
 * @return 
 */
int text_height(text_font_t * const font, int const point, char* const text) {
    /* @72dpi*/
    int h = -1;
    FT_GlyphSlot slot;
    FT_Error error = FT_Set_Char_Size(font->face, point * 64, 0, 72, 0); /* set character size */
    slot = font->face->glyph;
    int num_chars = strlen(text);
    for (int n = 0; n < num_chars; n++) {
        /* load glyph image into the slot (erase previous one) */
        error = FT_Load_Char(font->face, text[n], FT_LOAD_RENDER);
        if (error)
            continue; /* ignore errors */
        if (h < slot->advance.y / 64) {
            h = slot->advance.y / 64;
        }
    }
    return h;
}

/**
 * 
 * @param font
 * @param point
 * @param text
 * @return 
 */
int text_width(text_font_t * const font, int const point, char* const text) {
    /* @72dpi*/
    int w = 0;
    FT_GlyphSlot slot;
    FT_Error error = FT_Set_Char_Size(font->face, point * 64, 0, 72, 0); /* set character size */
    slot = font->face->glyph;
    int num_chars = strlen(text);
    for (int n = 0; n < num_chars; n++) {
        /* load glyph image into the slot (erase previous one) */
        error = FT_Load_Char(font->face, text[n], FT_LOAD_RENDER);
        if (error)
            continue; /* ignore errors */
        w += slot->advance.x / 64;
    }
    return w;
}

/* EOF */
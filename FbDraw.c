#include "FbDraw.h"
#include "FbJpg.h"

static unsigned char oR;
static unsigned char oG;
static unsigned char oB;
static unsigned char oA;
static long int location;

/**
 *
 */
rgba_t* fb_get_pixel(int const x, int const y) {
    if (x > 0 && x < fb_screen_width() && y > 0 && y < fb_screen_height()) {
        location = (x * fb_pixel_size()) + (y * fb_line_size());
        // offset by the current page start
        location += fb_page_start();
        unsigned char* o;
        fb_get_at(location, &o);
        rgba_t* c = mapRgba(o[2], o[1], o[0], o[3]);
        return c;
    }
    return NULL;
}

/**
 * 
 * @param x
 * @param y
 * @param c
 */
void fb_draw_pixel(int const x, int const y, rgba_t * const c) {
    //si x et y dans l'écran et si alpha > 0
    if (x > 0 && x < fb_screen_width() && y > 0 && y < fb_screen_height() && c->full[3] > 0) {
        location = (x * fb_pixel_size()) + (y * fb_line_size());
        // offset by the current page start
        location += fb_page_start();
        //si alpha<255, alphablend
        if (c->full[3] < 0xFF) {
            unsigned char o[4];
            fb_get_at(location, o);
            o[2] = (c->ra + o[2] * ~c->aa) >> 8;
            o[1] = (c->ga + o[1] * ~c->aa) >> 8;
            o[0] = (c->ba + o[0] * ~c->aa) >> 8;
            o[3] = 0xFF;
            fb_put_at(location, o);
        } else {
            //si alpha de 255
            fb_put_at(location, c->full);
        }
    }
}



    /**
     16bpp putpixel

                int b = 10;
                int g = (x - 100) / 6; // A little green
                int r = 31 - (y - 100) / 16; // A lot of red
                unsigned short int t = r << 11 | g << 5 | b;
     *((unsigned short int*) (fbp + location)) = t;
 
     */

    // draw a rectangle filled with given color

    void fb_fill_rect(int const x0, int const y0, int const w, int const h, rgba_t * const c) {        
        //without alphablend
        //int location = (x0 * fb_pixel_size()) + (y0 * fb_line_size());
        //fb_put_rect(location,w,h,c->full);
        //With alphablend        
        for (int y = 0; y < h; y++) {
            fb_stroke_line(x0, y0 + y, x0 + w, y0 + y, c);
        }
    }

    // draw a rectangle in given color

    void fb_stroke_rect(int const x, int const y, int const w, int const h, rgba_t * const c) {
        fb_stroke_line(x, y, x + w, y, c);
        fb_stroke_line(x + w, y, x + w, y + h, c);
        fb_stroke_line(x + w, y + h, x, y + h, c);
        fb_stroke_line(x, y + h, x, y, c);
    }

    // clear the screen - fill whole screen with given color
    //@todo: using color to clear

    void fb_clear_screen(rgba_t * const c) {
        fb_clear(c);
    }

    void fb_stroke_line(int x0, int y0, int const x1, int const y1, rgba_t * const c) {
        int dx = x1 - x0;
        dx = (dx >= 0) ? dx : -dx; // abs()
        int dy = y1 - y0;
        dy = (dy >= 0) ? dy : -dy; // abs()
        int sx;
        int sy;
        if (x0 < x1)
            sx = 1;
        else
            sx = -1;
        if (y0 < y1)
            sy = 1;
        else
            sy = -1;
        int err = dx - dy;
        int e2;
        int done = 0;
        while (!done) {
            fb_draw_pixel(x0, y0, c);
            if ((x0 == x1) && (y0 == y1))
                done = 1;
            else {
                e2 = 2 * err;
                if (e2 > -dy) {
                    err = err - dy;
                    x0 = x0 + sx;
                }
                if (e2 < dx) {
                    err = err + dx;
                    y0 = y0 + sy;
                }
            }
        }
    }

    void fb_stroke_circle(int const x0, int const y0, int const r, rgba_t * const c) {
        int x = r;
        int y = 0;
        int radiusError = 1 - x;

        while (x >= y) {
            // top left
            fb_draw_pixel(-y + x0, -x + y0, c);
            // top right
            fb_draw_pixel(y + x0, -x + y0, c);
            // upper middle left
            fb_draw_pixel(-x + x0, -y + y0, c);
            // upper middle right
            fb_draw_pixel(x + x0, -y + y0, c);
            // lower middle left
            fb_draw_pixel(-x + x0, y + y0, c);
            // lower middle right
            fb_draw_pixel(x + x0, y + y0, c);
            // bottom left
            fb_draw_pixel(-y + x0, x + y0, c);
            // bottom right
            fb_draw_pixel(y + x0, x + y0, c);

            y++;
            if (radiusError < 0) {
                radiusError += 2 * y + 1;
            } else {
                x--;
                radiusError += 2 * (y - x + 1);
            }
        }
    }

    void fb_fill_circle(int const x0, int const y0, int const r, rgba_t * const c) {
        int x = r;
        int y = 0;
        int radiusError = 1 - x;

        while (x >= y) {
            // top
            fb_stroke_line(-y + x0, -x + y0, y + x0, -x + y0, c);
            // upper middle
            fb_stroke_line(-x + x0, -y + y0, x + x0, -y + y0, c);
            // lower middle
            fb_stroke_line(-x + x0, y + y0, x + x0, y + y0, c);
            // bottom 
            fb_stroke_line(-y + x0, x + y0, y + x0, x + y0, c);

            y++;
            if (radiusError < 0) {
                radiusError += 2 * y + 1;
            } else {
                x--;
                radiusError += 2 * (y - x + 1);
            }
        }
    }

    /**
     * Tourne autour pour remplir
     */
    static void flood_loop(int x, int y, rgba_t * const dst_c, rgba_t * src_c) {
        int fillL, fillR, i;
        int in_line = 1;
        rgba_t* t;
        /* find left side, filling along the way */
        fillL = fillR = x;
        while (in_line) {
            fb_draw_pixel(fillL, y, dst_c);
            fillL--;
            if (fillL < 0) {
                break;
            }
            t = fb_get_pixel(fillL, y);
            in_line = rgba_compare(t, src_c);
            free(t);
        }
        fillL++;
        /* find right side, filling along the way */
        in_line = 1;
        while (in_line) {
            fb_draw_pixel(fillR, y, dst_c);
            fillR++;
            if (fillR > fb_screen_width()) {
                break;
            }
            t = fb_get_pixel(fillR, y);
            in_line = rgba_compare(t, src_c);
            free(t);
        }
        fillR--;
        /* search top and bottom */
        for (i = fillL; i <= fillR; i++) {
            t = fb_get_pixel(i, y - 1);
            if (y > 0 && (rgba_compare(t, src_c)))
                flood_loop(i, y - 1, dst_c, src_c);
            free(t);
            t = fb_get_pixel(i, y + 1);
            if (y < fb_screen_height() - 1 && (rgba_compare(t, src_c)))
                flood_loop(i, y + 1, dst_c, src_c);
            free(t);
        }
    }

    /**
     *
     */
    void fb_fill(int x, int y, rgba_t * const c, rgba_t * const s) {
        flood_loop(x, y, c, s); //Tourne autour
        fb_draw_pixel(x, y, c); //change la couleur du point de depart
    }

/* 
 * File:   Rgba.h
 * Author: root
 *
 * http://www.yellowpipe.com/yis/tools/hex-to-rgb/color-converter.php
 * 
 * Created on 11 février 2016, 09:13
 */

#ifndef RGBA_H
#define RGBA_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>

    typedef struct rgba {
        short ra;
        short ga;
        short ba;
        short aa;
        unsigned char full[4];
    } rgba_t;

    int rgba_compare(rgba_t * const a, rgba_t * const b);
    rgba_t* mapRgba(unsigned char const r, unsigned char const g, unsigned char const b, unsigned char const a);
    rgba_t* mapRgb(unsigned char const r, unsigned char const g, unsigned char const b);
    void unmapRgba(rgba_t* rbga);
    void rgba_alpha_blend(rgba_t * c, int const a);

#ifdef __cplusplus
}
#endif

#endif /* RGBA_H */


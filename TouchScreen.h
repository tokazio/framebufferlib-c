#ifndef TOUCHSCREEN_H
#define TOUCHSCREEN_H

#ifdef __cplusplus
extern "C" {
#endif

#include <fcntl.h>
#include <stdio.h>
#include <fcntl.h> 
#include <unistd.h> 
#include <linux/input.h>
#include <pthread.h>
#include "FrameBuffer.h"


    typedef void (*onClickCallback)(int, int, int, int);

    int ts_open(char* const fileName);
    void startTouch(onClickCallback onClick);
    void stopTouch(void);
    void ts_close(void);
    void ts_print_info(void);

#ifdef __cplusplus
}
#endif

#endif /* TOUCHSCREEN_H */

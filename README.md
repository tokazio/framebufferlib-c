# FrameBufferLib #

C Dynamic library for framebuffer (/dev/fb0) access in 32bpp mode.
+ touchscreen control (/dev/input/event0) access.

Only tested on Ubuntu(x64) and Odroid C1+(ARM).

Netbeans 8.0.1 project.

### Functionalities ###

Use page flip (double buffering) and vsync.

Can draw primitives (in one color):

* pixel (put & get)
* line (stroke)
* circle (stroke and fill)
* rect (stroke and fill)
* flodd fill
* clear to black screen (background color work in progress)
* text_fill (using libfreetype6)
* jpg_fill (with basic scale - using libjpeg8)

Possibility for software alpha blending (slow, work in progress).

### Dependancies ###

* build-essential (sudo apt-get install build-essential)
* libjpeg8 (sudo apt-get install libjpeg-dev)
* libfreetype6 (sudo apt-get install libfreetype6-dev)

### Referencies ###
http://www.ummon.eu/Linux/API/Devices/framebuffer.html

http://stackoverflow.com/questions/694080/how-do-i-read-jpeg-and-png-pixels-in-c-on-linux

http://stackoverflow.com/questions/4996777/paint-pixels-to-screen-via-linux-framebuffer

http://raspberrycompote.blogspot.fr/2014/04/low-level-graphics-on-raspberry-pi-text.html